Module: Chat Gabbly
Author: Mike Carter <www.ixis.co.uk/contact>


Description
===========
Adds an AJAX powered chat room to your site using the gabbly.com service and
provides unlimited chat rooms.

This module also supresses the advertising placed in the chat room a user
enters.


Requirements
============
Users of the chat room will require a JavaScript capable web browser. There is
no fall back if JavaScript is not available.


Installation
============
Copy the 'chat_gabbly' module directory in to your Drupal
modules directory as usual.

Chat permissions need to be allocated to user roles as required.


Configuration
=============

Admin settings are minimal for Chat Gabbly. By default your chat rooms will be published on
Gabbly.com when users enter them. To prevent this ensure you set the privacy
option is checked.

A menu item is made available in the menu navigation block, you can disable it or
move it in to another menu block such as the Primary Links if desired, or just
disable it. Use the standard menu admin interface to do this.

A block is also made available (you just need to add it to a region in your theme)
that allows you to specify a list of chat rooms to host.


Usage
=====
A default chat room is available to go right out of the box. Navigate
to ?q=chat to experience Gabbly.

You don't need to use the block to add additional rooms, just create a new room
topic by adding it to the end of the path. For example:-

?q=chat/Drupal+Rocks
?q=chat/Chatterbox

The toolbar buttons in the chat window are as follows:
footprints - stop messages when people enter and leave the chat room
bell - turn sounds on and off.

If you allow anonymous users to participate in your chat room, then an optional
box will be available on the chat toolbar so that they can give them selves a
more descriptive username.  If the user is logged in they will be only allowed
to use their Drupal account name.